import { shallowMount } from '@vue/test-utils';
import App from '~/App.vue';

describe('App', () => {
	test('should have 3 child components', () => {
		const wrapper = shallowMount(App);
		wrapper.findAllComponents('*');
		expect(wrapper.findAllComponents('*').length).toEqual(3);
	});
});

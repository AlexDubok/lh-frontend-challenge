import { shallowMount } from '@vue/test-utils';
import HotelCard from '~/components/HotelCard.vue';

describe('HotelCard component', () => {
	test('should render props', () => {
		const wrapper = shallowMount(HotelCard, {
			props: {
				name: 'Test hotel',
				distance: 43,
				price: {
					amount: 99,
					currency: 'USD',
				},
				info: 'some test info',
			},
		});

		expect(wrapper.find('.content__title').text()).toEqual('Test hotel');
		expect(wrapper.find('.content__description').text()).toEqual('43 km from the city center');
		expect(wrapper.find('.content__price').text()).toEqual('99 USD');
		expect(wrapper.find('.content__info').text()).toEqual('some test info');
	});

	test('should round distance value', () => {
		const wrapper = shallowMount(HotelCard, {
			props: {
				name: 'Test hotel',
				distance: 43.72438999,
			},
		});

		expect(wrapper.find('.content__description').text()).toEqual('43.72 km from the city center');
	});
});

// @ts-check
const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const { DefinePlugin } = require('webpack');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const { VueLoaderPlugin } = require('vue-loader');
require('dotenv').config();

const isDevelopment = process.env.NODE_ENV === 'development';

module.exports = {
	mode: isDevelopment ? 'development' : 'production',
	watch: isDevelopment,
	devtool: isDevelopment ? 'inline-source-map' : false,
	devServer: {
		contentBase: './dist',
		injectHot: true,
	},
	entry: {
		index: path.resolve(__dirname, './src/index.js'),
	},
	output: {
		publicPath: '/',
		filename: isDevelopment ? '[name].bundle.js' : '[name].[contenthash].bundle.js',
		path: path.resolve(__dirname, 'dist'),
	},

	module: {
		rules: [
			{
				test: /\.vue$/,
				loader: 'vue-loader',
			},
			{
				test: /\.scss$/,
				use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
			},
			{
				test: /\.css$/,
				use: [MiniCssExtractPlugin.loader, 'css-loader'],
			},
			{
				test: /\.svg$/,
				use: ['vue-loader', 'vue-svg-loader'],
			},
		],
	},
	plugins: [
		new DefinePlugin({
			'process.env.G_API_KEY': JSON.stringify(process.env.G_API_KEY),
		}),
		new VueLoaderPlugin(),
		new MiniCssExtractPlugin({
			filename: `[name].${isDevelopment ? '' : '[contenthash]'}.css`,
		}),
		new HtmlWebpackPlugin({
			title: 'Limehome frontend challenge',
			template: path.resolve(__dirname, './public/index.html'),
		}),
		new CopyPlugin({
			patterns: [
				{
					from: path.resolve(__dirname, './public/assets'),
					to: path.resolve(__dirname, './dist'),
				},
			],
		}),
		new BrowserSyncPlugin(
			// BrowserSync options
			{
				host: 'localhost',
				port: 3000,
				proxy: 'http://localhost:8080/',
			},
			{
				reload: false,
			}
		),
	],
};

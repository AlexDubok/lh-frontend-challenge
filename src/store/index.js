import { createStore } from 'vuex';
import hotels from './modules/hotels';

export default createStore({
	modules: {
		hotels,
	},
});


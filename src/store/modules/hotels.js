import hotelsFixture from '../../../fixtures/limehome-hotels.json';

const getPhotoUrl = (PlacePhoto, params) => {
	const photoObj = { ...PlacePhoto, ...params };
	return photoObj.getUrl();
};

const getMockPhoto = (i) => {
	const { url } = hotelsFixture[i % hotelsFixture.length].image;
	return `${url}?w=200&fit=crop&auto=format`;
};

const dumpHotel = (place, i) => ({
	id: place.place_id,
	distance: place.rating * 0.1, // todo: receive distance from API or use Google places Distance API
	name: place.name,
	position: {
		lat: place.geometry.location.lat(),
		lng: place.geometry.location.lng(),
	},
	price: {
		amount: Math.round(place.rating * 16),
		currency: 'EUR',
	},
	image: {
		url: place.photos ? getPhotoUrl(place.photos[0], { width: 200 }) : getMockPhoto(i),
	},
});

export default {
	namespaced: true,

	state() {
		return {
			hotels: [],
			selected: {},
		};
	},

	mutations: {
		setHotels: (state, hotelsList) => {
			state.hotels = hotelsList;
		},

		addHotels: (state, hotelsList) => {
			state.hotels.push(...hotelsList);
		},

		setActive: (state, hotel) => {
			state.selected = hotel;
		},
	},

	actions: {
		addHotels: ({ commit }, list) => {
			commit('addHotels', list.map(dumpHotel));
		},

		clearHotels: ({ commit }) => {
			commit('setHotels', []);
		},

		selectHotel: ({ commit }, hotel) => {
			commit('setActive', hotel);
		},
	},
};

export default () => new Promise((resolve) => {
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(
			({ coords }) => {
				resolve({
					lat: coords.latitude,
					lng: coords.longitude,
				});
			},
			() => {
				resolve();
			}
		);
	} else {
		resolve();
	}
});


export const mapSettings = {
	disableDefaultUI: true,
	streetViewControl: false,
	backgroundColor: '#aadaff',
};

export const placesSearchConfig = {
	radius: 10000,
	keyword: 'hotel',
	openNow: true,
	type: ['lodging'],
};

export const POINT_MARKER_ICON_CONFIG = {
	url: '../assets/home-icon.svg',
};

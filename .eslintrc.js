module.exports = {
	extends: [
		'airbnb-base',
		'plugin:vue/base',
		'plugin:vue/essential',
		'plugin:vue/strongly-recommended',
		'plugin:vue/recommended',
	],
	env: {
		browser: true,
		node: true,
		es6: true,
		jest: true,
	},
	rules: {
		'no-multiple-empty-lines': ['error', {
			max: 1,
			maxBOF: 1,
			maxEOF: 1,
		}],
		'no-prototype-builtins': 'off',
		'no-console': 'warn',
		indent: ['error', 'tab'],
		'no-tabs': 'off',
		'no-param-reassign': ['error', { props: false }],
		'max-len': ['error', 120, 4, {
			ignoreUrls: true,
			ignoreComments: false,
			ignoreRegExpLiterals: true,
			ignoreStrings: true,
			ignoreTemplateLiterals: true,
		}],
		'global-require': 'off',
		'func-names': 'off',
		curly: 'error',
		'comma-dangle': [
			'error',
			{
				arrays: 'always-multiline',
				objects: 'always-multiline',
				imports: 'never',
				exports: 'never',
				functions: 'never',
			},
		],
		'class-methods-use-this': 'off',
		'import/no-extraneous-dependencies': [
			'warn', {
				devDependencies: true,
				optionalDependencies: true,
				peerDependencies: true,
			},
		],
		'import/no-unresolved': 'warn',
		'import/extensions': [
			'error',
			'always',
			{
				js: 'never',
				json: 'ignorePackages',
			},
		],
		'no-plusplus': ['error', { allowForLoopAfterthoughts: true }],
		'no-debugger': process.env.NODE_ENV !== 'development' ? 'error' : 'off',
		'object-curly-newline': ['error', {
			ObjectExpression: {
				multiline: true, minProperties: 3, consistent: true,
			},
			ObjectPattern: { multiline: true, minProperties: 5 },
			ImportDeclaration: 'never',
			ExportDeclaration: { multiline: true, minProperties: 4 },
		}],
		'newline-per-chained-call': ['error', { ignoreChainWithDepth: 2 }],
		'vue/max-attributes-per-line': [2, {
			singleline: 3,
		}],
		'vue/html-indent': ['error', 'tab'],
		'vue/html-closing-bracket-newline': ['error', {
			singleline: 'never',
			multiline: 'always',
		}],
		'vue/html-closing-bracket-spacing': ['error', {
			startTag: 'never',
			endTag: 'never',
			selfClosingTag: 'always',
		}],
		'vue/no-multiple-template-root': 'off',
		'vue/order-in-components': ['error', {
			order: [
				'el',
				'name',
				['props', 'propsData'],
				'parent',
				'functional',
				['delimiters', 'comments'],
				['components', 'directives', 'filters'],
				'LIFECYCLE_HOOKS',
				'extends',
				'mixins',
				'inheritAttrs',
				'model',
				'data',
				'computed',
				'watch',
				'methods',
				['template', 'render'],
				'renderError',
			],
		}],
		'vue/html-self-closing': ['error', {
			html: {
				void: 'never',
				normal: 'never',
				component: 'always',
			},
			svg: 'never',
			math: 'always',
		}],
		'no-underscore-dangle': [
			'error',
			{
				allow: ['__INITIAL_STATE__'],
			},
		],

	},
};

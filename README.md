# limehome-frontend-challenge

### Tech stack
Vue, vuex, webpack, jest, Google Maps JS API

### Project details
* `src/components/googleMap/` - components that use Google API
* Data shared between components is stored in the vuex store
* Slider uses native horizontal scroll to swipe between elements
* Colors stored in css variables for possible theming

Map component searches for hotels close to the current map center. Search results are rendered incrementally page by page. 
If geolocation access is granted on the initial load, the map will render the current device location.

### Launch instructions
To utilize google API, environment variable `G_API_KEY` should be provided.
- `npm start` - run project in development mode.
- `npm run build` - build for production
- `npm t` - run tests